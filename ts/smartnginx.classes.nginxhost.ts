import * as plugins from './smartnginx.plugins';
import * as paths from './smartnginx.paths';
import * as snippets from './smartnginx.snippets';

import { SmartNginx } from './smartnginx.classes.smartnginx';

import { IHostConfig } from './interfaces/hostconfig';

export enum hostTypes {
  reverseProxy
}

/**
 * manages a single nginx host
 */
export class NginxHost implements IHostConfig {
  /**
   * smartnginxInstance this NginHost belongs to
   */
  public smartnginxInstance: SmartNginx;

  public hostName: string; // the host name e.g. domain name
  public destination: string;
  public destinationPort: number;
  public configString: string; // the actual host config file as string
  public privateKey: string;
  public publicKey: string;

  constructor(smartnginxInstanceArg: SmartNginx, optionsArg: IHostConfig) {
    this.smartnginxInstance = smartnginxInstanceArg;
    this.hostName = optionsArg.hostName;
    this.destination = optionsArg.destination;
    this.destinationPort = optionsArg.destinationPort;
    this.privateKey = optionsArg.privateKey;
    this.publicKey = optionsArg.publicKey;
  }

  /**
   *
   * @param certInstanceArg
   */
  public async deploy() {
    const filePathConfig = plugins.path.join(paths.nginxHostDirPath, `${this.hostName}.conf`);
    const filePathPrivate = plugins.path.join(
      paths.nginxHostDirPath,
      `${this.hostName}.private.pem`
    );
    const filePathPublic = plugins.path.join(paths.nginxHostDirPath, `${this.hostName}.public.pem`);

    // writeConfig
    this.configString = snippets.getHostConfigString(
      this.hostName,
      this.destination,
      this.destinationPort
    );
    plugins.smartfile.memory.toFsSync(this.configString, filePathConfig);

    // write ssl
    plugins.smartfile.memory.toFsSync(this.privateKey, filePathPrivate);
    plugins.smartfile.memory.toFsSync(this.publicKey, filePathPublic);
  }
}
