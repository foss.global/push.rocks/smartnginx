import * as plugins from './smartnginx.plugins';
import * as paths from './smartnginx.paths';
export let getBaseConfigString = (defaultProxy: string) => {
  const baseConfig = plugins.smartstring.indent.normalize(`
		user www-data;
		worker_processes auto;
		pid /run/nginx/nginx.pid;

		events {
			worker_connections 768;
			# multi_accept on;
		}

		http {
			server_names_hash_bucket_size 128;

			##
			# Basic Settings
			##

			sendfile on;
			tcp_nopush on;
			tcp_nodelay on;
			keepalive_timeout 65;
			types_hash_max_size 2048;
			# server_tokens off;

			# server_names_hash_bucket_size 64;
			# server_name_in_redirect off;

			include /etc/nginx/mime.types;
			default_type application/octet-stream;

			##
			# SSL Settings
			##

			ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
			ssl_prefer_server_ciphers on;

			##
			# Logging Settings
			##

			access_log /var/log/nginx/access.log;
			error_log /var/log/nginx/error.log;

			##
			# Gzip Settings
			##

			gzip on;
			gzip_disable "msie6";

			# gzip_vary on;
			# gzip_proxied any;
			# gzip_comp_level 6;
			# gzip_buffers 16 8k;
			# gzip_http_version 1.1;
			# gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

			##
			# Virtual Host Configs
			##

			server {
				listen *:80 default_server;
				server_name selfsigned.git.zone;
				rewrite        ^ ${defaultProxy} redirect;
			}
	
			server {
				listen *:443 ssl default_server;
				server_name selfsigned.git.zone;
				ssl_certificate ${paths.nginxHostDirPath}/default.public.pem;
				ssl_certificate_key ${paths.nginxHostDirPath}/default.private.pem;
				rewrite        ^ ${defaultProxy} redirect;
			}

			include ${paths.nginxHostDirPath}/*.conf;
			include /etc/nginx/sites-enabled/*;
		}
		daemon off;
	`);
  return baseConfig;
};

export let getHostConfigString = (
  hostNameArg: string,
  destinationIpArg: string,
  destinationPortArg = 80
) => {
  const hostConfig = plugins.smartstring.indent.normalize(`
		upstream ${hostNameArg} {
			keepalive 100;
			server ${destinationIpArg}:${destinationPortArg};
		}

		server {
			# The keepalive parameter sets the maximum number of idle keepalive connections
  		# to upstream servers that are preserved in the cache of each worker process. When
			# this number is exceeded, the least recently used connections are closed.
			listen *:80 ;
			server_name ${hostNameArg};
			rewrite        ^ https://${hostNameArg}$request_uri? permanent;
		}

		server {
			listen *:443 ssl;
			server_name ${hostNameArg};
			ssl_certificate ${paths.nginxHostDirPath}/${hostNameArg}.public.pem;
			ssl_certificate_key ${paths.nginxHostDirPath}/${hostNameArg}.private.pem;
			location / {
				proxy_http_version      1.1;
				proxy_buffering         off;
				proxy_redirect					off;
				proxy_set_header        X-Real-IP $remote_addr;
				proxy_set_header        X-Forwarded-For $remote_addr;
				proxy_set_header        X-Forwarded-Proto $scheme;
				proxy_next_upstream error timeout http_404 http_429 http_500 http_502;
				proxy_next_upstream_tries 5;
				proxy_pass              http://${hostNameArg};
			}
		}
	`);
  return hostConfig;
};
